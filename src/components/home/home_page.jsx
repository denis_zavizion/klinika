import React from 'react';

import Slider from "../Slide";
import HomeBlock1 from "./home_block_1";
import HomeBlock2 from "./home_block_2";
import HomeBlock3 from "./home_block_3";
import HomeBlock4 from "./home_block_4";
import HomeBlock5 from "./home_block_5";

const  HomePage = () => {
    return <div>
        <Slider/>
        <HomeBlock1/>
        <HomeBlock2/>
        <HomeBlock3/>
        <HomeBlock4/>
        <HomeBlock5/>
    </div>
}
export default HomePage;