import React from 'react';
import team from '../../access/img/team.png';
import addImg from '../../access/img/addImg.svg';
const  HomeBlock4 = () => {
    return <section className="teamAndAppointment">
        <div className="container">
            <div className="row">
                <div className="col-lg-7">
                    <div className="team">
                        <h2 className="sectionTitle">Meet Our Team</h2>
                        <div className="team_img">
                            <img src={team} alt="" />
                        </div>
                    </div>
                </div>
                <div className="col-lg-5">
                    <div className="appointment">
                        <h2 className="sectionTitle center">Make an Appointment</h2>
                        <form action="">
                            <div className="form-group">
                                <input type="text" name="fullName" placeholder="Full name" />
                            </div>
                            <div className="form-group">
                                <input type="tel" name="phoneNumber" placeholder="Phone number" />
                            </div>
                            <div className="form-group">
                                <input type="email" name="emailAddress" placeholder="Email Address" />
                            </div>
                            <div className="form-group">
                                <input type="text" name="appointmentDate" placeholder="Appointment Date" />
                            </div>
                            <div className="form-group">
                                <textarea name="message" placeholder="Message"></textarea>
                                <button className="btn addImgBtn" type="button">
                                    <img src={addImg} alt="" />
                                </button>
                            </div>
                            <button className="btn submitBtn" type="submit">SUBMIT REQUEST</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
}
export default HomeBlock4;