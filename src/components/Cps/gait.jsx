import React from 'react';



const  Gait = () => {
    return <section className="gait">
        <div className="container">
            <h2 className="sectionTitle">Gait Scan and Foot Pain in Croydon</h2>
            <h3 className="sectionSubtitle">Do you suffer with foot pain, or unresolved leg pain?</h3>
            <p>
                We have a high-tech piece of equipment which is a foot scan pressure plate that takes hundreds of data
                of
                force and pressure measurements from your feet while walking or running, and then analyses biomechanical
                problems with the legs.
            </p>
            <p>
                In many cases, biomechanic problems can be resolved with exercises or physiotherapy, but sometimes a
                patient
                may request foot orthortics, or insoles, to help position their foot in a better position.
            </p>
            <p>
                The foot insoles and orthotics are also really useful for unresolved foot pain, such as plantar
                fasciitis,
                Morton's neuroma, recurrent ankle sprains, heel pain, Achilles tendonitis, recurrent calf strains and
                stress
                fractures in the leg and foot bones.
            </p>
            <p>
                To book an appointment, just call 0208 662 5059 and say you would like a foot scan or biomechanics
                analysis
                and one of our physio's will see you. The whole team have been specifically trained in the provision of
                feet
                biomechanics, and some of our team are very expert in foot pain.
            </p>
            <div className="bg__grey">
                We offer very affordable foot insoles, or for serious athletes, we offer bespoke insoles which get made
                specifically for each patient depending on their foot size, position, pressure, and imbalance. These
                devices
                fit into most types of shoes, and can considerably reduce pain in the feet, hips, and knees remarkably
                quickly. The bespoke insoles are more expensive, of course, because they are made to measure, and these
                usually provide the best benefits
            </div>
            <h3 className="sectionExTitle">The cost of our foot insoles and orthotics are £279.00 and this includes the
                cost of the consultation.</h3>
        </div>
    </section>
}
export default Gait;