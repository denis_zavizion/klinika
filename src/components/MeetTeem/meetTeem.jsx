import React from 'react';

import team from '../../access/img/team.png';
import person_1 from '../../access/img/person_1.png';
import person_2 from '../../access/img/person_2.png';
import person_3 from '../../access/img/person_3.png';
import person_4 from '../../access/img/person_4.png';
import person_6 from '../../access/img/person_6.png';
import person_7 from '../../access/img/person_7.png';
import person_8 from '../../access/img/M77A8823.jpeg';

const  meetTeem = () => {
    return <div>
        <section className="meetTheTeam">
            <div className="container">
                <h2 className="sectionTitle">Meet the Team</h2>
                <h3 className="sectionSubtitle">Basically, you're in safe hands from a well-trained team.</h3>
                <div className="aboutTeam">
                    <div className="aboutTeam__image">
                        <img src={team} alt="" />
                    </div>
                    <div className="aboutTeam__desc">
                        From left to right, Niamh, Tim, Kay, Amy, Peter and Richard are all members of either the Health Care
                        Professions Council or the General Osteopathic Council, with which we must be registered to call
                        ourselves physiotherapists or osteopaths. We are also registered with the Chartered Society of
                        Physiotherapy and PhysioFirst. We are also members of the Institute of Osteopathy, ACPSEM, ACPOHE, OSCA and a few other regulatory bodies.
                    </div>
                </div>
            </div>
        </section>

        <div className="aboutPerson">
            <div className="container">

                <div className="person">
                    <div className="person__desc">
                        <div className="person__name">Will</div>
                        <div className="person__excerpt">
                            <p>Will is an experienced Chartered Physiotherapist who is also the Lead Physiotherapist for our Wandsworth and Lambeth Team, and Lead for Whitgift School.</p>
                        </div>
                    </div>
                    <div className="person__img">
                        <img src={person_8} alt="" />
                    </div>
                </div>

                <div className="person">
                    <div className="person__desc">
                        <div className="person__name">Tim Allardyce</div>
                        <div className="person__excerpt">
                            <p>
                                Tim has spent seven years training as an osteopath and physiotherapist and in February
                                2005
                                he set up Addington Palace Osteopathy and Physiotherapy Clinic. He specialises in
                                treating
                                elite athletes and currently his clientele includes Olympians, Football Internationals,
                                and
                                World Champions. He has a reputation for developing good athletes into elite athletes,
                                and
                                also works with European Tour Golfers. During his spare time (which isn't very much), he
                                enjoys competitive sailing.
                            </p>
                        </div>
                    </div>
                    <div className="person__img bigOne">
                        <img src={person_1} alt="" />
                    </div>
                </div>
                <div className="person">
                    <div className="person__desc">
                        <div className="person__name">Niamh Wright</div>
                        <div className="person__excerpt">
                            <p>Niamh graduated from the British school of Osteopathy after successfully completing a 4
                                year
                                degree course. She is an excellent structural osteopath who has experience treating a
                                range of
                                people including expectant mothers, sports people and children. Niamh has a special
                                interest in
                                health and fitness related injuries, and has a background in competitive sports
                                including
                                swimming and tennis. She also enjoys coaching young children to swim. She is also a
                                qualified
                                Pilates instructor.</p>
                            <p>Niamh is almost always fully booked, and is an experienced and competent therapist</p>
                        </div>
                    </div>
                    <div className="person__img">
                        <img src={person_2} alt="" />
                    </div>
                </div>
                <div className="person">
                    <div className="person__desc">
                        <div className="person__name">Peter Tylinski</div>
                        <div className="person__excerpt">
                            <p>
                                Peter is an established member of the Croydon Physio team, and has made a huge
                                impression on so
                                many patients over the last few years. He is a chartered physiotherapist who graduated
                                in 2009.
                                He worked for over 3 years in a highly specialist military condition centre treating
                                back pains.
                                Most of his patients were elite Air Force. Treating back pain has become his main
                                interest and
                                now he uses numerous ways like massage, exercise and physiotherapy to help patients in
                                recovery.
                            </p>
                            <p>
                                Peter has also been a qualified massage therapist since 2006 specialising in many types
                                of
                                massage treatments: segmentary, deep tissue, sports, Swedish, reflexology, lymphatic
                                drainage,
                                acupressure, remedial. He has previously worked with a National sailing team as their
                                physio.
                            </p>
                            <p>
                                As a physiotherapist Peter has experience in using electrotherapy equipment, light
                                therapy,
                                ultrasound, hydrotherapy and manual techniques. He also enjoys developing and reviewing
                                treatment programmes, tailored to individual patient’s needs.
                            </p>
                        </div>
                    </div>
                    <div className="person__img">
                        <img src={person_3} alt="" />
                    </div>
                </div>
                <div className="person">
                    <div className="person__desc">
                        <div className="person__name">Kay Allardyce</div>
                        <div className="person__excerpt">
                            <p>
                                Kay has been an Osteopath since 2005 and trained at The British College of Osteopathic
                                Medicine.
                                She is also qualified in modern acupuncture/ dry needling, which is used for some
                                musculo
                                skeletal problems. Kay will use cranial osteopathy in some treatments, especially when
                                treating
                                infants and babies. Kay is also a qualified Pilates instructor. Kay will do a thorough
                                assessment initially, looking at posture, flexibility and spinal range of motion.
                                Specific
                                exercises are then prescribed helping to strengthen and mobilise according to your
                                individual
                                body needs.
                            </p>
                            <p>
                                Classes are either on a one to one basis or small groups of 3-4. This is to allow Kay to
                                observe
                                and correct technique throughout the whole session, reducing the risk of inaccurate and
                                potentially injurious performance, which so many patients have reported in large group
                                exercise
                                classes.
                            </p>
                        </div>
                    </div>
                    <div className="person__img">
                        <img src={person_4} alt="" />
                    </div>
                </div>
                <div className="person">
                    <div className="person__desc">
                        <div className="person__name">Helen Waters</div>
                        <div className="person__excerpt">
                            <p>
                                Helen graduated as a Physiotherapist from York St John University in 2010. She has since
                                worked
                                in the rehabilitation and medico-legal industry for 5 years; initially in a clinical
                                governance
                                role developing an audit and quality framework within a private healthcare company, and
                                later
                                moving into a management position with operational responsibility, focussing on the
                                development
                                of her clinical team and service delivery. During this time she also did part-time
                                sports
                                Physiotherapy with sports teams and charity events across the UK. She has now returned
                                to
                                clinical practice full time at Surrey Physio and looks forward to continuing to develop
                                within
                                the team.
                            </p>
                            <p>
                                She is a keen sportsperson and has played badminton at a competitive level for over 15
                                years,
                                representing Northumberland county and, more recently, Yorkshire and local Surrey
                                leagues.
                            </p>
                            <p>
                                Outside of work she enjoys photography and loves the outdoors; spending as much time as
                                she can
                                camping, walking and travelling the country. Although now working and living in Surrey
                                she
                                remains a true northerner at heart!
                            </p>
                        </div>
                    </div>
                    <div className="person__img">

                    </div>
                </div>
                <div className="person">
                    <div className="person__desc">
                        <div className="person__name">Jackson Ellis</div>
                        <div className="person__excerpt">
                            <p>
                                Jackson qualified from St Georges University in London in 2011, and since then he worked
                                in the
                                NHS treating both adults and children. He has also worked with semi-professional men’s
                                football
                                teams, as well as for Professional Championship Club Millwall FC's youth Academy.
                                Jackson has
                                also trained and received qualifications in the instruction of Pilates for Low back Pain
                                and
                                Scoliosis. He also has a special interest in paediatric and adolescent musculoskeletal
                                conditions, and Sports Injuries. Jackson is a fantastic member of the team, and we are
                                really
                                pleased to have him at Croydon Physio
                            </p>
                        </div>
                    </div>
                    <div className="person__img">
                        <img src={person_6} alt="" />
                    </div>
                </div>
                <div className="person">
                    <div className="person__desc">
                        <div className="person__name">Elizabeth (Lizzy) Donnelly</div>
                        <div className="person__excerpt">
                            <p>
                                Lizzy Donnelly is one of our newest physiotherapists and has previously worked in the
                                NHS. Her first physiotherapy job was working for St Georges mental health trust. Her
                                role was to work in the community and in the outpatient department. She has experience
                                treating a variety of patients including patients in the community and in the outpatient
                                setting. Lizzy decided to gain more experience working in the MSK outpatient department
                                and was fortunate enough to work for Queen Mary’s hospital in Sidcup She took
                                hydrotherapy classes in the local pool and covered other classes including Pilates.
                                Lizzy has also worked for Hammersmith and Fulham Ruby club delivering pitch side
                                assistance and treating acute sport injuries.
                            </p>
                            <p>
                                Outside of the clinical setting Lizzy is passionate about sport and plays football for
                                AFC Phoenix and competes in athletics for Herne Hill Harriers. Lizzy is passionate about
                                health promotion and the benefits they can have to physical and mental health.
                            </p>
                        </div>
                    </div>
                    <div className="person__img">
                        <img src={person_7} alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
}
export default meetTeem;