import React from 'react';

import payCard_1 from '../../access/img/payCard_1.svg';
import payCard_2 from '../../access/img/payCard_2.svg';
import payCard_3 from '../../access/img/payCard_3.svg';
import {NavLink} from "react-router-dom";
import Dropdown from '../module/Dropdown/Choose/dropdown';


const  cost = () => {

    const chooses1 = [{'id': '1', 'value': 'Physiotherapy 40.00 GBR'}];
    const chooses2 = [{'id': '1', 'value': 'GBR'}];

    return <section className="cost">
        <div className="container">
            <h2 className="sectionTitle">Cost</h2>
            <h3 className="sectionSubtitle">
                The cost of treatment is £40 for the consultation, and £40 for subsequent treatments. We charge £40 flat
                rate for all insurance. Treatment usually lasts up to 30 minutes. Should you require an hour, our charge
                is
                £70. Some treatments with Tim are 20 minutes (Saturdays and late evenings) and the cost of this is £35.
            </h3>
            <div className="payment">
                <div className="payWay">
                    <div className="payWay__title">Choose treatment</div>
                    {/*<select name="treatment" id="treatment">*/}
                    {/*    <option value="0" hidden></option>*/}
                    {/*    <option value="1">Physiotherapy 40.00 GBR</option>*/}
                    {/*</select>*/}

                    {/*<div className="nice-select" tabIndex="0"><span className="current"></span>*/}
                    {/*    <ul className="list">*/}
                    {/*        <li data-value="0" className="option"></li>*/}
                    {/*        <li data-value="1" className="option selected">Physiotherapy 40.00 GBR</li>*/}
                    {/*    </ul>*/}
                    {/*</div>*/}

                    <Dropdown choose={chooses1} name={'treatment'} />

                    <button className="btn payment__btn">Pay now</button>
                    <div className="payCards">
                        <NavLink to="/">
                            <img src={payCard_1} alt="" />
                        </NavLink>
                        <NavLink to="/">
                            <img src={payCard_2} alt="" />
                        </NavLink>
                        <NavLink to="/">
                            <img src={payCard_3} alt="" />
                        </NavLink>
                    </div>
                </div>
                <div className="payWay">
                    <div className="payWay__title">Choose treatment</div>
                    {/*<select name="customAmount" id="customAmount">*/}
                    {/*    <option value="0" hidden></option>*/}
                    {/*    <option value="1">GBR</option>*/}
                    {/*</select>*/}
                    {/*<div className="nice-select" tabIndex="0"><span className="current"></span>*/}
                    {/*    <ul className="list">*/}
                    {/*        <li data-value="0" className="option selected"></li>*/}
                    {/*        <li data-value="1" className="option">GBR</li>*/}
                    {/*    </ul>*/}
                    {/*</div>*/}
                    <Dropdown choose={chooses2} name={'customAmount'} />

                    <button className="btn payment__btn">Pay now</button>
                    <div className="payCards">
                        <NavLink to="/">
                            <img src={payCard_1} alt="" />
                        </NavLink>
                        <NavLink to="/">
                            <img src={payCard_2} alt="" />
                        </NavLink>
                        <NavLink to="/">
                            <img src={payCard_3} alt="" />
                        </NavLink>
                    </div>
                </div>
                <div className="cheque">
                    <h3 className="cheque__title">Insurance codes:</h3>
                    <p>BUPA Physio: 80009032</p>
                    <p>BUPA Osteopathy: 30032104</p>
                    <p>AXA/PPP Physio: ZZ01300</p>
                    <p>AXA/PPP Osteopathy: TA00652</p>
                    <p>Aviva: 600033966</p>
                    <p>WPA Physio: 3926528</p>
                    <p>WPA Osteopathy: 920272028</p>
                </div>
            </div>
            <div className="paragraph color__grey">PLEASE REMEMBER YOUR INSURANCE IS YOUR RESPONSIBILITY. If the
                insurance company does not pay, you will be responsible for any excess.
            </div>
            <div className="paragraph color__red">PLEASE NOTE AT CROYDON PHYSIOTHERAPY WE ARE VERY BUSY AND WE DO
                REQUEST 24 HOURS' NOTICE IF YOU WISH TO CANCEL, OR WE CHARGE OUR FULL RATE FOR MISSED APPOINTMENTS.
            </div>
            <p>
                We do provide Home Visits - the cost varies depending on your location and the time it takes us to get
                to you, but starts at £70 for local call-outs.
            </p>
            <p>
                We are covered by all major insurance companies, including...
            </p>
            <p>
                BUPA, AXA PPP, Aviva, Simply Health, Royal and Sun Alliance, Standard Life, Pruhealth, WPA, CS
                Healthcare, Cigna UK, Groupama, and Benendon.
            </p>
        </div>
    </section>
}
export default cost;