import React from 'react';

import testimonials from '../../access/img/testimonials.png';

const  Testimonials = () => {
    return <section className="testimonials">
        <div className="container">
            <h2 className="sectionTitle">Client Testimonials</h2>
            <div className="testimonial__item">
                <div className="testimonial__desc">
                    <p>
                        "Having had a lower back injury since my early twenties, I have visited a number of physios,
                        osteopaths and chiropractors over the past forty years. The best treatment by far has been that
                        provided through Croydon Physio. On my first visit, I was walking with a stick and had been in
                        constant pain for weeks. Regular treatment has transformed the situation, and returned me to
                        long
                        periods of time that are totally pain-free. The professionalism and attention to individual
                        needs
                        that Tim Allardyce and his colleagues shows is exemplary and I gratefully recommend them most
                        warmly."
                    </p>
                    <div className="testimonial__author">Dr Christopher Barnett, Headmaster, Whitgift School, June
                        2017
                    </div>
                </div>
                <div className="testimonial__image">
                    <img src={testimonials} alt="" />
                </div>
            </div>
            <div className="testimonial__item">
                <div className="testimonial__desc">
                    <p>
                        "Having had a lower back injury since my early twenties, I have visited a number of physios,
                        osteopaths and chiropractors over the past forty years. The best treatment by far has been that
                        provided through Croydon Physio. On my first visit, I was walking with a stick and had been in
                        constant pain for weeks. Regular treatment has transformed the situation, and returned me to
                        long
                        periods of time that are totally pain-free. The professionalism and attention to individual
                        needs
                        that Tim Allardyce and his colleagues shows is exemplary and I gratefully recommend them most
                        warmly."
                    </p>
                    <div className="testimonial__author">Dr Christopher Barnett, Headmaster, Whitgift School, June
                        2017
                    </div>
                </div>
                <div className="testimonial__image">
                    <img src={testimonials} alt="" />
                </div>
            </div>
            <div className="testimonial__item">
                <div className="testimonial__desc">
                    <p>
                        "Having had a lower back injury since my early twenties, I have visited a number of physios,
                        osteopaths and chiropractors over the past forty years. The best treatment by far has been that
                        provided through Croydon Physio. On my first visit, I was walking with a stick and had been in
                        constant pain for weeks. Regular treatment has transformed the situation, and returned me to
                        long
                        periods of time that are totally pain-free. The professionalism and attention to individual
                        needs
                        that Tim Allardyce and his colleagues shows is exemplary and I gratefully recommend them most
                        warmly."
                    </p>
                    <div className="testimonial__author">Dr Christopher Barnett, Headmaster, Whitgift School, June
                        2017
                    </div>
                </div>
                <div className="testimonial__image">
                    <img src={testimonials} alt="" />
                </div>
            </div>
            <div className="testimonial__item">
                <div className="testimonial__desc">
                    <p>
                        "Having had a lower back injury since my early twenties, I have visited a number of physios,
                        osteopaths and chiropractors over the past forty years. The best treatment by far has been that
                        provided through Croydon Physio. On my first visit, I was walking with a stick and had been in
                        constant pain for weeks. Regular treatment has transformed the situation, and returned me to
                        long
                        periods of time that are totally pain-free. The professionalism and attention to individual
                        needs
                        that Tim Allardyce and his colleagues shows is exemplary and I gratefully recommend them most
                        warmly."
                    </p>
                    <div className="testimonial__author">Dr Christopher Barnett, Headmaster, Whitgift School, June
                        2017
                    </div>
                </div>
                <div className="testimonial__image">
                    <img src={testimonials} alt="" />
                </div>
            </div>
            <div className="testimonial__item">
                <div className="testimonial__desc">
                    <p>
                        "Having had a lower back injury since my early twenties, I have visited a number of physios,
                        osteopaths and chiropractors over the past forty years. The best treatment by far has been that
                        provided through Croydon Physio. On my first visit, I was walking with a stick and had been in
                        constant pain for weeks. Regular treatment has transformed the situation, and returned me to
                        long
                        periods of time that are totally pain-free. The professionalism and attention to individual
                        needs
                        that Tim Allardyce and his colleagues shows is exemplary and I gratefully recommend them most
                        warmly."
                    </p>
                    <div className="testimonial__author">Dr Christopher Barnett, Headmaster, Whitgift School, June
                        2017
                    </div>
                </div>
                <div className="testimonial__image">
                    <img src={testimonials} alt="" />
                </div>
            </div>
        </div>
    </section>
}
export default Testimonials;