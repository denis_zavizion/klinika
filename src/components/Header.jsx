import React, {useState} from 'react';
import facebook from '../access/img/facebook.svg';
import twitter from '../access/img/twitter.svg';
import skype from '../access/img/skype.svg';
import logo from '../access/img/logo2.jpg';


import {NavLink} from "react-router-dom";
class Header extends React.Component {

    state = {
        activeClass: false
    };
    handleClick = (button) => {button == 'open' ? this.setState({ activeClass: true }) : this.setState({ activeClass: false }) };

    render() {
        return (
            <header>
                <div className="headerMain">
                    <div className="container">
                        <a href="index.html" className="headerMain__logo">
                            <img src={logo} alt=""/>
                        </a>
                        <div className="headerMain__contacts">
                            <a href="tel:" className="headerMain__contacts--phone">
                                0208 651 3315
                            </a>
                            <div className="headerMain__contacts--socialnetworks">
                                <a href="#/">
                                    <img src={facebook} alt=""/>
                                </a>
                                <a href="#/">
                                    <img src={twitter} alt=""/>
                                </a>
                                <a href="#/">
                                    <img src={skype} alt=""/>
                                </a>
                            </div>
                        </div>

                        {/*<div className="menuIcon" active= {this.state.active} onClick={() => this.handleClick()} >*/}
                        <div className="menuIcon" onClick={() => this.handleClick('open')} >
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>

                {/*<div className='headerNav {setState}' onClick={handleClick}>*/}
                {/*<div className={'headerNav' + isActive} >*/}
                <div className={this.state.activeClass ? 'headerNav active' : 'headerNav'} >
                    <div className="container" >

                        {/*<div className="menuClose" onClick={ActiveClose}>*/}
                        <div className="menuClose"  onClick={() => this.handleClick('close')}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <ul className="headerNav__ul">

                            <li><NavLink to="/" className="selected">Welcome</NavLink></li>
                            <li><NavLink to="/news">News</NavLink></li>
                            <li><NavLink to="/about">About Us</NavLink></li>
                            <li><NavLink to="/meet_teem">Meet the Team</NavLink></li>
                            <li><a href="" className="down">Croydon Physio Services</a>
                                <ul className="submenu">
                                    <li><NavLink to="/pilates">Pilates</NavLink></li>
                                    <li><NavLink to="/gait">Gait Scan</NavLink></li>
                                </ul>
                            </li>
                            <li><NavLink to="/contact">Contact us</NavLink></li>
                            <li><NavLink to="/cost">Cost</NavLink></li>
                            <li><NavLink to="/testimonials">Testimonials</NavLink></li>
                            <li><NavLink to="/olympics">Olympics</NavLink></li>

                        </ul>
                        <div className="headerMain__contacts">
                            <a href="tel:" className="headerMain__contacts--phone">
                                0208 651 3315
                            </a>
                            <div className="headerMain__contacts--socialnetworks">
                                <a href="#/">
                                    <img src={facebook} alt=""/>
                                </a>
                                <a href="#/">
                                    <img src={twitter} alt=""/>
                                </a>
                                <a href="#/">
                                    <img src={skype} alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        );
    }

}

// var setActive = '';



// handleClick = () => {
//     setActive = 'active';
// };


export default Header;