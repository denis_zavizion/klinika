import React from 'react';
import banner1 from '../access/img/slide/M77A5360b.JPG.jpg';
import banner2 from '../access/img/slide/M77A5958.jpeg';
import banner3 from '../access/img/slide/M77A8979.jpeg';
import banner4 from '../access/img/slide/M77A8984.jpeg';
const  Slider = () => {
    return <section className="banner">
        <div className="banner-carousel">
            <div className="carousel-cell">
                <img src={banner1} alt="" />
            </div>
            {/*<div className="carousel-cell">*/}
            {/*    <img src={banner2} alt="" />*/}
            {/*</div>*/}
            {/*<div className="carousel-cell">*/}
            {/*    <img src={banner3} alt="" />*/}
            {/*</div>*/}
            {/*<div className="carousel-cell">*/}
            {/*    <img src={banner4} alt="" />*/}
            {/*</div>*/}
        </div>
        <div className="container">
            <div className="bannerText">
                <h2 className="bannerTitle">A friendly, professional and affordable service</h2>
            </div>
        </div>
    </section>
}
export default Slider;